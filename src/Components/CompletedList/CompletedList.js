import React, { Component } from 'react';
import './CompletedList.css';
import CompletedItem from './Components/CompletedItem';

class CompletedList extends Component {
    getCompletedList() {
        const { completedList } = this.props;
        if (completedList.length === 0) return <div>You have no completed tasks</div>
    
        return completedList.map((value,index) => (
          <CompletedItem
            id={value.id}
            text={value.text}
            number={index}
            key={value.id}  
          />
        ));
      }
    render() {
        return (
             <div className="completed-list">
                <h2>Done:</h2>
                {this.getCompletedList()}
             </div>    
        );
    }
}

export default CompletedList;
