import React, { Component } from 'react';
import './CompletedItem.css';

class CompletedItem extends Component {
    render() {
        const { id, text } = this.props; 
        console.log('this props', this.props)
        return (
            <li className='list-element animated fadeIn'>
                <div className="text">
                   <span>{text}</span>
                </div>
            </li>
        );
    }
}

export default CompletedItem;
