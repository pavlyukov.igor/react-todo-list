import React, { Component } from 'react';
import './Header.css';


class Header extends Component {
    handleAddClick = () => {
        const {addTodo} = this.props;
        const {value} = this.input;

        addTodo(value);
        this.input.value = '';
    }

    handleEnterClick = (event) => {
        if (event.key === 'Enter') { 
            const {addTodo} = this.props;
            const {value} = this.input;

            addTodo(value);
            this.input.value = '';
        }
    }

    render() {
        return (
            <header className="header">
                <h2>Todo List</h2>
                <div className="form-group">
                    <input className="input" onKeyPress={this.handleEnterClick} ref={input => {this.input = input} } type="text" />
                    <button className="btn" onClick={this.handleAddClick}>ADD</button>
                </div>
            </header>
        );
    }
}

export default Header;
