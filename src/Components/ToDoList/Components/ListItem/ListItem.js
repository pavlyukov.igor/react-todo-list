import React, { Component } from 'react';
import './ListItem.css';

class ListItem extends Component {
  state = {
    editMode: false
  }

  listChangeInput = (event) => {  
    if (event.key === 'Enter' || !event.key) {
      console.log('click')
      this.editValue();
    }
  }

  setEditMode = () => {
    const {text} = this.props;

    this.setState({
      editMode: true
    }, () => {
      this.input.value = text;
      this.input.focus();
    })
  }

  editValue = () => {
    const { id, editItem } = this.props;
    const { value } = this.input;

    if (value !== '') editItem(id, value);

    this.setState({
      editMode: false
    })
  }

  getTextOrInput = () => {
    const {number, text} = this.props; 
    const {editMode} = this.state;

    if (editMode) return (
      <input 
        className="input-edit animated fadeIn"
        ref={(input) => this.input = input} 
        onBlur={this.listChangeInput} 
        onKeyPress={this.listChangeInput} 
      />
    )

    return <span onClick={this.setEditMode}>{number + 1}. {text}</span>
  }

  render() {
    const { id, done, remove, switchStatus } = this.props; 
    return (
        <li className={`list-element ${done ? 'complete' : ''} animated fadeIn`}>
            <div className="text">
              <label htmlFor="checkbox" className="custom-checkbox"></label>
              <input 
                id="checkbox" 
                className="checkbox" 
                type="checkbox" 
                value={done} 
                onClick={() => switchStatus(id)}>
              </input>

              {this.getTextOrInput()}
            </div>
            <div className="options">
              <button className="close" onClick={() => remove(id)}></button>
            </div>
        </li>
    );
  }
}

export default ListItem;