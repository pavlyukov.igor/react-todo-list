import React, { Component } from 'react';
import ListItem from './Components/ListItem';
import {SortableContainer, SortableElement, arrayMove} from 'react-sortable-hoc';

const SortableItem = SortableElement(ListItem);
class ToDoList extends Component {
  getList() {
    const { todoList, removeItem, switchStatus, editItem } = this.props;
    if (todoList.length === 0) return <div>You have no tasks</div>

    return todoList.map((value,index) => (
      <SortableItem
        id={value.id}
        text={value.text}
        number={index}
        index={index}
        done={value.done}
        key={value.id}
        remove={removeItem}
        switchStatus={switchStatus}
        editItem={editItem}
      />
    ));
  }

  render() {
    return (
      <ul>
         {this.getList()}
      </ul>
    );
  }
}

ToDoList.defaultProps = {
  todoList: [],
  removeItem: () => null,
  switchStatus: () => null,
  editItem: () => null
}

export default ToDoList;
