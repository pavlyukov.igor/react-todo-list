import React, { Component } from 'react';
import Header from './Components/Header';
import ToDoList from './Components/ToDoList';
import CompletedList from './Components/CompletedList';
import {SortableContainer, SortableElement, arrayMove} from 'react-sortable-hoc'; 

import './App.css';

const SortableList = SortableContainer(ToDoList);
class App extends Component {
  state = {
    todoList: [
      {id: Math.random(), text: 'buy milk', done: false},
      {id: Math.random(), text: 'read a book about ReactJS', done: false},
      {id: Math.random(), text: 'create react app, todo list', done: false},
      {id: Math.random(), text: 'and do something else', done: false},
    ],
    completedList: []
  }

  onSortEnd = ({oldIndex, newIndex}) => {
    this.setState({
      todoList: arrayMove(this.state.todoList, oldIndex, newIndex),
    });
  };

  addNewItem = (text) => {
    if (text === '') {
      return;
    }

    const newTodo = {
      id: Math.random(),
      text: text,
      done: false
    }

    this.setState({
      ...this.state,
      todoList: [...this.state.todoList, newTodo]
    })
  }

  removeItem = (id) => {
    const newTodoList = [...this.state.todoList].filter(v => v.id !== id);

    this.setState({
      ...this.state,
      todoList: [...newTodoList]
    })
  }

  editItem = (id, value) => {
    const elementIndex = [...this.state.todoList].findIndex(v => v.id === id);

    const newTodoList = [...this.state.todoList];
    newTodoList[elementIndex].text = value;

    this.setState({
      ...this.state,
      todoList: [...newTodoList]
    })
  }

  switchStatus = (id) => {
    const elementIndex = [...this.state.todoList].findIndex(v => v.id === id); 
    const newTodoList = [...this.state.todoList]; 
    newTodoList[elementIndex].done = !newTodoList[elementIndex].done;
    
    this.setState({
      ...this.state,
      todoList: [...newTodoList]
    })

    this.switchCompletedStatus(id, elementIndex, newTodoList);
  }

  switchCompletedStatus = (id, elementIndex, newTodoList) => {

    const elementCompleteIndex = [...this.state.completedList].findIndex(v => v.id === id);
    const newCompleteTodoList = [...this.state.completedList]; 
    newCompleteTodoList.splice(elementCompleteIndex, 1); 
    
    this.setState({
      completedList: [...newCompleteTodoList]
    })
    
    if (newTodoList[elementIndex].done) {
      let doneItem = newTodoList[elementIndex]; 
      this.setState({
        completedList: [...this.state.completedList, doneItem]
      })
    } 
  }

  render() {
    return (
      <div className="app"> 
         <Header addTodo={this.addNewItem}/>
         <SortableList 
          {...this.state} 
          removeItem={this.removeItem}
          switchStatus={this.switchStatus}
          editItem={this.editItem}
          items={this.state.todoList} onSortEnd={this.onSortEnd}
         />
         <CompletedList 
            completedList={this.state.completedList} 
          /> 
      </div>
    );
  }
}


export default App;

